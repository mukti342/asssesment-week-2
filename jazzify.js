const jazzify = (cord) => {

    for (let i = 0; i < cord.length; i++) {
        
        if (cord[i].charAt(cord[i].length - 1) != 7) {
            cord[i] += 7
        }
        
    }
    return cord;

};

const input = ["G7", "Am", "C"];

console.log(jazzify(input));
