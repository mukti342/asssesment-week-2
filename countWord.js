const countWord = (count, input) => {
    return input.toLowerCase().split(count.toLowerCase()).length - 1;

};

const wordToCount = "dog";
const input = "cat and dog. cat and sheep";

console.log(countWord(wordToCount, input));
