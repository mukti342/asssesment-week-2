const capitalToFront = (str) => {
    let strArray = str.split('');
    let strArrAwal =[];
    let strArrSisa =[];
    let strArrGabungan =[];

    for (let i = 0; i < strArray.length; i++) {
        if (strArray[i] == strArray[i].toUpperCase()) {
            strArrAwal += strArray[i];
            
        }else{
            strArrSisa += strArray[i];
        }
        
    }
    strArrGabungan = [...strArrAwal, ...strArrSisa];
    return strArrGabungan.join('');
};

const input = "hEllO";

console.log(capitalToFront(input));
