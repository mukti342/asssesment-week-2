const oddEven = (number) => {
    // jika angka habis di bagi 2 maka angka adalah angka genap
    // jika angka tidak habis di bagi 2 maka angka adlah angka ganjil

    if (number % 2 === 0 ) {
        return `${number} is even number`
    } else {
        return `${number} is odd number`
    }


};

const input = 5;

console.log(oddEven(input));
