const squareEveryDigit = (angka) => {

    let squerAngka = '';
    for (let i = 0; i < angka.length; i++) {
        squerAngka = squerAngka + angka[i] * angka[i];
        
    }
    return squerAngka;

};

const input = "1234";

console.log(squareEveryDigit(input));
